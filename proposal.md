# Games Lab 2017 #
## Game Pitch ##

**
Team members:
**

This document contains the idea of our future game, denotes the main aspects our team is going to focus on and the goals our team is going to reach in order to make a perfect game for the Game Practicum.

### Overview ###

If we leave all our doubts behind and forget about unnecessary details, we can quickly summarize our game as a **cooperative dungeon crawler** with addition of some roguelike elements. On the one hand, we tried to pick the genre so that we can relate to rich background in game industry: this way we do not risk to end up reinventing the wheel. By formulating the genre of our future creation as "dungeon crawler" we stand on a solid ground: we can be certain that even in case when our new concepts fail to impress the public, we are still getting a decent, playable and fun product.

On the other hand, we intend to add experimental features to our game to make a really unique product out of it. Moreover, these features is precisely what is going to define the final look of the game and unveil the *Together* topic -- topic that the organisers had assigned to this game practicum session.  Our team aims to extract as much fun as possible from the limited resources we possess and raise the level of cooperation beyond the mere interaction between the **Crawlers**. To do so, we introduced a concept of the **Master** (?) that guides every other player throughout the dungeon. His role is distinct, fun, and extremely responsible -- he needs to keep an eye on all the players at once, help them and show them a way to the victory.

### Gameplay of Crawlers ###

Crawlers are the PC players who can actually reach the finish line and end the game. They can run across the map and explore it, but in one distinct moment of time they can see only what's happening around them, in their locality, be it a dark chamber or a narrow hallway. Every single door, every single corner for them is a mystery: anything can hide there. In a whole, their gameplay can resemble you some popular games like Diablo: they interact with objects in their range of reachability, directly inflict damage to enemies, collect loot, and so on. In any moment of time they can accidentally step into a room full of angry monsters and die... Or can they?

### Gameplay of Master ###

They cannot, if their Master won't allow them to! In our game the Crawlers are not totally blind since they are guided by a godlike being, mounted with Oculus Rift!

What does the Master's gameplay look like? It looks more like a strategy. The Master needs to take care of all the Crawlers, which might be scattered throughout the dungeon. The Master does not only see the entire map from above, but also has a specific set of actions that are unique and cannot be performed by the Crawlers. These actions may include the ability to alter the dungeon itself: for instance, create the additional walls or break the existing ones. The Master might also be able to "buff" and "debuff" the players, heal them, or do whatever a godlike creature can do. And, most importantly, he can give messages to the Crawlers. Information is a weapon and the Master possess it.

### Roguelike elements ###

In standard dungeon crawlers players are randomly wandering across some map, lazily fighting with the monsters and collecting the loot, until they accidentally find the boss and defeat her. In our case this lazy gameplay is unacceptable! Since we do not have enough assets to make the long gameplay entertaining enough, what we really want is to make a short but really impressive game, when none of the players (and especially not the Master) loses their focus.

It would be fantastic if the potential gamers could gather in the cozy evening and cover the entire game in a couple of hours. To achieve this, we borrowed some features from roguelike games. The levels are going to be concise so the Master could track the entire map and so the Crawlers would not be tired of exploring it. Every game should be distinct from each other and provide different experiences to players thoughout different runs, making the game replayable. This can be achieved by generating content of the levels and perks/items of the players randomly. If the perks/items are designed well enough, they can alter the style of playing rapidly, and their combination can be even more interesting due to caleidoscopic effect!

### Together ###

The last, but the most important thing of the gameplay we would like to define here is the players' goals and how they are achieved. The very essence of our game is well described by a single word: *Together*. We intend to design the game so the Crawlers can complete it only together and under the guidance of the Master. Hence, the success in our game consists of two parts: the teamplay of the Crawlers themselves and the mutual interaction between each Crawler and the Master. We have discussed how to build the level layout in order to expose our ideas in the best way and come up to the following scenario:

- The entire game consists of several levels, next level is available after clearing the previous one.
- At the beginning of each level, the Crawlers are scattered in the dungeon, starting from different corners of it.
- To clear the level, the Crawlers need to find and defeat the boss that guards the exit somewhere in the dungeon. 
- Boss can be (most probably) defeated only by all of the Crawlers together, in a teamplay.
- The Crawlers do not know anything in the beginning. They do not know where is the boss and they do not know where are the other Crawlers.
- The Master knows almost everything. He sees the entire map from above and knows where are the Crawlers now.
- The dungeon is not uniformly fair, unlike in many games. This feature forces the Master to intervene, for instance, to prevent the Crawlers to go inside some hostile but bare areas, or to help them to avoid traps.
- Time not always plays in favor of the Crawlers. New monsters are spawned in the dungeon, and the Master should analyse the situation quickly and effectively organise other players.
- Some parts of the dungeon can be accessed only via the tight cooperation between several Crawlers, coordnated by the Master.

The aforementioned list outlines how we see the large scale process of playing our game. At the beginning of every level, the Master should think about making the way of the Crawlers to the centre of the map avoiding the variety of obstacles, and somehow manage them to **gather together, in one place**, as it is the essential condition of defeating the boss and moving to the next level. Only together, combining their strengths, the Crawlers can survive and kill the boss.

The Crawlers never can see the entire picture, but they must feel the intents of their Master in order to survive and accomplish their goals. Having a very specific set of abilities, the Master not only shows them a way, but also helps in many different ways. It should be mentioned that the Crawlers might get into a situation when they cannot survive unless their Master intervenes, and intervenes quickly.

Finally, the Master has not got unlimited power (although he is godlike). He needs to think carefully of how not to spend his abilities in vain. Moreover, sometimes it is not the Master who helps the Crawlers, but it is the Crawlers who need to help the Master to regain his wasted powers.

### Technical details ###

Here is the list of technical stuff we need to implement in order to get what we want:

1. **Procedurally generate** interesting maps.
2. **Render** 3D dungeon and their inhabitants.
3. Establish proper **networking** that allows data to effectively circulate between the Crawlers and the Master.
4. Carefully implement the **game logic**.
5. Provide the **visuals** for our game.

Finally, the most interesting part is: we need to integrate Oculus Rift so that the Master might be able to see the dungeon laying under him in **artificial reality**. This will make interaction between the Master and the dungeon extremely fun and enjoyable.

### Goals ###

1. Successfuly implement technical stuff mentioned in the previous section.
2. Design the Crawlers' and the Master's set of abilities and perks carefully to engage the teamplay.
3. Dstribute the responsibilities between the players so neither the Master nor the Crawlers get bored while playing.
4. Design the ideal game balance tso the game has an optimal difficulty and the gameplay does not gets annoying.

### Crabs ###

1. 🦀
